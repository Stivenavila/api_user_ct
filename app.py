from flask import Flask, jsonify, request, json
from utils.db_utils import Database
import base64

app = Flask(__name__)


# GET Data Route
@app.route('/api/client', methods=['GET'])
def GetClient():
    conn = Database().con()
    cursor = conn.cursor()
    cursor.execute("select * from users_ct;")
    result = {'data': [dict(zip(tuple(cursor.keys()), i)) for i in cursor]}
    return jsonify(result)


@app.route('/api/client/<string:client_id>', methods=['GET'])
def GetClient_ID(client_id):
    conn = Database().con()
    cursor = conn.cursor()
    cursor.execute("select * from users_ct where client_id =%d " 
    % int(client_id))
    result = {'data': [dict(zip(tuple(cursor.keys()), i)) for i in cursor]}
    return jsonify(result)


# POST Data Route
@app.route('/api/client', methods=['POST'])
def PostClient():
    conn = Database().con()
    cursor = conn.cursor()
    dict_string = json.dumps(request.json)
    dict = json.loads(dict_string)
    for i in dict:
        client_id = i['client_id']
        country_id = i['country_id']
        country = i['country']
        source_id = i['source_id']
        source_name = i['source_name']
        has_own_credentials = i['has_own_credentials']
        username = i['username']
        email = i['email']
        password = i['password']
        cript_pass = password
        message_bytes = cript_pass.encode('ascii')
        base64_bytes = base64.b64encode(message_bytes)
        base64_cript_pass = base64_bytes.decode('ascii')
        active = i['active']
        cursor.execute("insert into dev_analytics.users_ct (client_id,country_id,"
                        "country,source_id,source_name,"
                       "has_own_credentials,username, password, active, email)"
                       "values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", (
                           client_id, country_id, country, source_id, source_name, 
                           has_own_credentials, username,
                           base64_cript_pass, active, email))
    conn.commit()
    conn.close()
    return jsonify(status=200, message='status: Nuevo usuario añadido')


# PUT Data Route
@app.route('/api/client/<string:client_id>', methods=['PUT'])
def PutClient(client_id):
    conn = Database().con()
    cursor = conn.cursor()
    dict_string = json.dumps(request.json)
    dict = json.loads(dict_string)
    for i in dict:
        country_id = i['country_id']
        country = i['country']
        source_id = i['source_id']
        source_name = i['source_name']
        has_own_credentials = i['has_own_credentials']
        username = i['username']
        password = i['password']
        cript_pass = password
        message_bytes = cript_pass.encode('ascii')
        base64_bytes = base64.b64encode(message_bytes)
        base64_cript_pass = base64_bytes.decode('ascii')
        active = i['active']
        cursor.execute("update dev_analytics.users_ct set country_id='%s',country='%s'"
                        ", source_id='%s',source_name='%s',has_own_credentials='%s', "
                       "password='%s', username='%s', active='%s' where client_id=%d" 
                       "AND source_name='%s'" % (
                           country_id, country, source_id, source_name, has_own_credentials, 
                           base64_cript_pass, username, active,
                           int(client_id), source_name))
    conn.commit()
    conn.close()
    return jsonify(status=200, message='status: Cambio de usuario realizado')


# DELETE Data Route
@app.route('/api/client/<string:client_id>', methods=['DELETE'])
def DeleteClient(client_id):
    conn = Database().con()
    cursor = conn.cursor()
    dict_string = json.dumps(request.json)
    dict = json.loads(dict_string)
    for i in dict:
        source_name = i['source_name']
        cursor.execute(
            "delete from users_ct where client_id=%d AND source_name='%s'" % 
            (int(client_id), source_name))
    conn.commit()
    conn.close()
    return jsonify(status=200, message='status: usuario borrado con éxit')


if __name__ == '__main__':
    app.run(debug=True, port=4000)
