from datetime import datetime
from dotenv import load_dotenv
from os import getenv
import pymysql

load_dotenv()

ANALYTIC_DB = getenv('ANALYTICS_DB')
ANALYTIC_DB_HOST = getenv('ANALYTICS_DB_HOST')
ANALYTIC_DB_PORT = getenv('ANALYTICS_DB_PORT')
ANALYTIC_DB_USER = getenv('ANALYTICS_DB_USER')
ANALYTIC_DB_PASSWD = getenv('ANALYTICS_DB_PASSWD')


class times:
    host_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')


class Database:
    connection = None

    def con(self):
        try:
            if self.connection is None:
                self.connection = pymysql.connect(host=ANALYTIC_DB_HOST, port=3306,
                                                  user=ANALYTIC_DB_USER, passwd=ANALYTIC_DB_PASSWD,
                                                  db=ANALYTIC_DB)
        except ValueError as e:
            print('{}: error encontrado. {}:'.format(e, times))
        print("{}: Connection Successful".format(times.host_time))
        return self.connection
