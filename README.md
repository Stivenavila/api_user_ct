# Crear entorno virtual

```sh
python3 -m venv venv
```

# Activar entorno virtual

```sh
source venv/bin/activate
```

```sh
. venv/bin/activate
```

# Actualizar pip

```sh
python -m pip install --upgrade pip
```

# Instalar requerimientos

**Para desarrollo**

```sh
pip install -r requirements.txt
```

> **Note:** Es necesario activar el entorno virtual para usar este comando